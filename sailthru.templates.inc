<?php

function sailthru_templates_list_form()
{
  $form = array();

  $form['#tree'] = TRUE;

  $templates = sailthru_templates_get_list();

  foreach ($templates as $tid => $one)
  {
    $form['template:' . $tid] = array(
      '#item' => $one,
      'name' => array(
        '#markup' => $one['name'],
      ),
      'label' => array(
        '#markup' => $one['label'],
      ),
      'description' => array(
        '#markup' => $one['description'],
      ),
      'disabled' => array(
        '#markup' => $one['disabled'] ? 'Yes' : 'No',
      ),
      'tid' => array(
        '#type' => 'hidden',
        '#default_value' => $one['tid'],
        '#attributes' => array('class' => array('template-tid')),
      ),
      'parent' => array(
        '#type' => 'hidden',
        '#default_value' => $one['parent'],
        '#attributes' => array('class' => array('template-parent')),
      ),
      'weight' => array(
        '#type' => 'weight',
        '#delta' => 20,
        '#default_value' => $one['weight'],
        '#attributes' => array('class' => array('template-weight')),
      ),
      'depth' => array(
        '#type' => 'hidden',
        '#value' => $one['depth'],
      ),
    );
  }

  $form['actions'] = array('#type' => 'actions');

  if (element_children($form))
  {
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Save Changes',
    );
  }

  $form['#theme'] = 'sailthru_templates_list_form';

  return $form;
}

function sailthru_templates_list_form_submit($form, $form_state)
{
  $order = array_flip(array_keys($form_state['input'])); // Get the $_POST order.
  $form = array_merge($order, $form); // Update our original form with the new order.

  $updated_items = array();
  $fields = array('weight', 'parent');
  foreach (element_children($form) as $tid)
  {
    if (isset($form[$tid]['#item']))
    {
      $element = $form[$tid];
      // Update any fields that have changed in this menu item.
      foreach ($fields as $field)
      {
        if ($element[$field]['#value'] != $element[$field]['#default_value'])
        {
          $element['#item'][$field] = $element[$field]['#value'];
          $updated_items[$tid] = $element['#item'];
        }
      }
    }
  }

  if (count($updated_items))
  {
    foreach ($updated_items as $template)
    {
      sailthru_templates_update_weight($template['tid'], $template['weight']);
      sailthru_templates_update_parent($template['tid'], $template['parent']);
    }
  }
}

function theme_sailthru_templates_list_form($variables)
{
  $table_rows = array();
  $output = '';

  $header = array();
  $header[] = t('Name');
  $header[] = t('Label');
  $header[] = t('Description');
  $header[] = t('Weight');
  $header[] = t('Disabled');
  $header[] = t('Operations');

  $form = $variables['form'];


  foreach (element_children($form) as $tid)
  {
    if (isset($form[$tid]['#item']))
    {
      $this_row = array();

      $operations = l(t('Edit'), 'admin/config/services/sailthru/templates/edit/' . $form[$tid]['tid']['#value']) . ' | ' .
          l(t('Delete'), 'admin/config/services/sailthru/templates/delete/' . $form[$tid]['tid']['#value']);

      $this_row[] = theme('indentation', array('size' => $form[$tid]['depth']['#value'])) . drupal_render($form[$tid]['name']);
      $this_row[] = drupal_render($form[$tid]['label']);
      $this_row[] = drupal_render($form[$tid]['description']);
      $this_row[] = drupal_render($form[$tid]['weight']) . drupal_render($form[$tid]['parent']) . drupal_render($form[$tid]['tid']);
      $this_row[] = drupal_render($form[$tid]['disabled']);
      $this_row[] = $operations;

      $table_rows[] = array('data' => $this_row, 'class' => array('draggable'));
    }
  }

  if (count($table_rows))
  {
    drupal_add_tabledrag($table_id = 'sailthru_templates_list', $action = 'match', $relationship = 'parent', $group = 'template-parent', $subgroup = 'template-parent', $source = 'template-tid', $hidden = TRUE, $limit = 1);
    drupal_add_tabledrag($table_id = 'sailthru_templates_list', $action = 'order', $relationship = 'sibling', $group = 'template-weight');

  $output .= theme('table', array('header' => $header, 'rows' => $table_rows, 'attributes' => array('id' => 'sailthru_templates_list')));
  $output .= drupal_render_children($form);
  }
  else
  {
    $output .= t('There are no templates yet. <a href="@link">Sync template</a>.', array('@link' => url('admin/config/services/sailthru/templates/sync')));
  }


  return $output;
}

/**
 * Lists all the templates from the Sailthru system that were introduced to drupal
 */
function sailthru_templates_get_list()
{
  $query = db_select('sailthru_notification_templates', 'c')
      ->fields('c')
      ->condition('parent', 0, '=')
      ->orderBy('weight', 'ASC');

  $result = $query->execute();

  $templates = array();

  while ($row = $result->fetchAssoc())
  {
    $templates[$row['tid']] = $row;

    $query2 = db_select('sailthru_notification_templates', 'c')
        ->fields('c')
        ->condition('parent', $row['tid'], '=')
        ->orderBy('weight', 'ASC');

    $result2 = $query2->execute();

    while ($row2 = $result2->fetchAssoc())
    {
      $templates[$row2['tid']] = $row2;
    }
  }

  return $templates;
}

/**
 * Update single Sailthru template
 * @param $tid - template id, if given will do an update else it will do an insert
 * @param $weight - template weight
 */
function sailthru_templates_update_weight($tid, $weight)
{
  $res = db_update('sailthru_notification_templates')
      ->fields(array(
        'weight' => $weight,
      ))
      ->condition('tid', $tid, '=')
      ->execute();

  return $res;
}

/**
 * Update single Sailthru template
 * @param $tid - template id, if given will do an update else it will do an insert
 * @param $parent - template parent
 */
function sailthru_templates_update_parent($tid, $parent)
{
  $res = db_update('sailthru_notification_templates')
      ->fields(array(
        'parent' => $parent,
        'depth' => $parent ? 1 : 0,
      ))
      ->condition('tid', $tid, '=')
      ->execute();

  return $res;
}