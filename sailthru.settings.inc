<?php

/**
 * admin settings form for Sailthru API
 */
function sailthru_account_settings_form()
{
  $form = array();

  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Sailthru API Account settings'),
    '#description' => t('You can find these Setup parameters by going to: ' . l('my.sailthru.com/settings_api', 'https://my.sailthru.com/settings_api')),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );

  $form['settings']['sailthru_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Sailthru API key'),
    '#default_value' => variable_get('sailthru_api_key', ''),
    '#size' => 60,
    '#maxlength' => 32,
    '#description' => t('The api key provided by Sailthru, after registration'),
  );

  $form['settings']['sailthru_api_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Sailthru API secret'),
    '#default_value' => variable_get('sailthru_api_secret', ''),
    '#size' => 60,
    '#maxlength' => 32,
    '#description' => t('The api secret provided by Sailthru, after registration'),
  );

  $form['settings']['sailthru_api_uri'] = array(
    '#type' => 'textfield',
    '#title' => t('Sailthru API URI'),
    '#default_value' => variable_get('sailthru_api_uri', 'http://api.sailthru.com'),
    '#size' => 60,
    '#maxlength' => 64,
    '#description' => t('The URI to use for the Sailthru API'),
  );

  $form['developer'] = array(
    '#type' => 'fieldset',
    '#title' => t('Developer Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );

  $form['developer']['sailthru_developer_mode'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('sailthru_developer_mode', 1),
    '#title' => t('Toggle developer mode'),
    '#description' => t('Check this while you are developing the site. In this way all the emails will be sent to the developer account.'),
  );

  $form['developer']['sailthru_developer_email'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('sailthru_developer_email', variable_get('site_mail', '')),
    '#title' => t('Developer email'),
    '#description' => t('Add a developer email address to send all Sailthru emails, when using in developer mode.'),
  );

  return system_settings_form($form);
}